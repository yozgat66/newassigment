**About The Project**

**Contact**

Your Name - Mustafa Duyarer - mustafaduyarer@gmail.com

Project Link: https://gitlab.com/yozgat66/newassigment.git

This is a Cucumber BDD sample project which gives examples on how to run API automation test using RestAssured and for web automation it uses Selenium for it's core.

test-automation-weatherstack.com
The weatherstack API was built to deliver accurate weather data for any application and use case, from real-time and historical weather information all the way to 14-day weather forecasts, supporting all major programming languages.

Concepts Included
Cucumber BDD testing for API
Shared state across cucumber step definitions
Dependency injection
Page Object pattern
Common web page interaction methods
Commonly used test utility classes

This framework includes a feature file that prepared for
There is a feature file AdvancedSearch.feature named  and deneme.feature

Tools
Maven
Cucumber
JUnit
Selenium Webdriver
Cucumber Report
Selenium Grid

Requirements
In order to utilise this project you need to have the following installed locally:
Maven 4.0.0
Java 8 


Dependencies:
In order to utilise this project you need to have following dependencies in pom.xml
and you can find them https://mvnrepository.com
Selenium-version 3.141.59
webdrivermanager - version 4.1.0
cucumber-java - version 5.7.0
cucumber-junit - version 5.7.0
rest-assured  - version 4.3.3
gson  - 2.8.6

Usage
To run API tests only, navigate to com.assignment-runners directory and run:
CukesRunner

Reporting
Reports for each module are written into their respective /target directories after a successful run.
UI acceptance tests result in a HTML report for each feature in target\default-html-reports\index.html
In the case of test failures, a screen-shot of the UI at the point of failure is embedded into the report.

For Html report : http://localhost:63342/NewAssignment/target/default-html-reports/index.html?_ijt=cqmp428ocqha8q1g5it43r5jra

For cucumber report : http://localhost:63342/NewAssignment/target/cucumber-html-reports/overview-features.html

Below are the examples of Cucumber scenarios for API and UI that written in Gherkin language.

**Sample API Scenario**

 @test  @positive

  Scenario: TC_001 user access with valid access key

    Given user request "New York" weather status
    
    Then users verify the status code is 200.

**The glue codes for the scenario are these:**


     @Given("user request {string} weather status")
    public void user_request_weather_status(String string) {

         response = RestAssured.given().accept(ContentType.JSON)
                .queryParams("access_key", "13171e3eb6db6a4ee978b6cbbc73d615")
                .queryParams("query", string)
                .when().get("http://api.weatherstack.com/current");

    }

    @Then("users verify the status code is {int}.")
    public void users_verify_the_status_code_is(int num) {
        
        Assert.assertEquals(num,response.statusCode());
    }

**Running**

To run the project you can either run it directly from the CucumberRunner test runner class or using maven command mvn verify

**Installation**

Get a free API Key at https://weatherstack.com/
Clone the repo
git clone https://gitlab.com/yozgat66/newassigment.git
Install NPM packages
npm install













