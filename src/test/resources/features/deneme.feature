
Feature: Testing a REST API

  @test  @positive
  Scenario: TC_001 user access with valid access key
    Given user request "New York" weather status
    Then users verify the status code is 200.

  @test  @positive
  Scenario: TC_002 user search existing city in database
    Given user request "New York" weather status
    Then verify that "New York" city weather info is shown.

  @test  @positive
  Scenario:TC_003 user can request in different temperature unit
    When user request in Fahrenheit for London city.
    Then verify result should have Fahrenheit as temperature unit.



  @test @negative
  Scenario: TC_004 user access with invalid access key
    Given users go to the api url with invalid access key
    Then verify error code 101 in response body.

  @test @negative
  Scenario: TC_005 user search non existing city in database
    Given user request "NewYork" weather status
    Then verify that "NewYork" city weather info is not shown.
    And verify error info is "Your API request failed. Please try again or contact support."

  @test @negative
  Scenario: TC_006 user cannot access historical data with free plan
    Given user request "New York" weather status with "2019-01-21" historical date
    And "Your current subscription plan does not support historical weather data. Please upgrade your account to use this feature." info is given.

