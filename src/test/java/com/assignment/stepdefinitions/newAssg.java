package com.assignment.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

public class newAssg {
    Response response;

    @Given("user request {string} weather status")
    public void user_request_weather_status(String string) {

         response = RestAssured.given().accept(ContentType.JSON)
                .queryParams("access_key", "13171e3eb6db6a4ee978b6cbbc73d615")
                .queryParams("query", string)
                .when().get("http://api.weatherstack.com/current");

    }

    @Then("users verify the status code is {int}.")
    public void users_verify_the_status_code_is(int num) {
        Assert.assertEquals(num,response.statusCode());
    }

    @Then("verify that {string} city weather info is shown.")
    public void verify_that_city_weather_info_is_shown(String string) {

        response.then().assertThat().body("current", Matchers.hasKey("weather_descriptions"));

    }

    @When("user request in Fahrenheit for London city.")
    public void user_request_in_Fahrenheit_for_London_city() {

        response=RestAssured.given().accept(ContentType.JSON)
                .queryParam("access_key", "13171e3eb6db6a4ee978b6cbbc73d615").
                        queryParam("units","f").
                        queryParam("query", "London")
                .when().get("http://api.weatherstack.com/current");

    }

    @Then("verify result should have Fahrenheit as temperature unit.")
    public void verify_result_should_have_Fahrenheit_as_temperature_unit() {

        Assert.assertTrue(response.body().jsonPath().getString("request.unit").equals("f"));

    }

    @Given("users go to the api url with invalid access key")
    public void users_go_to_the_api_url_with_invalid_access_key() {
        response=RestAssured.given().accept(ContentType.JSON)
                .queryParam("access_key", "13171e3eb6db6a4dasdsad8b6cbbc73d615")
                .when().get("http://api.weatherstack.com/current");
    }

    @Then("verify error code {int} in response body.")
    public void verify_error_code_in_response_body(Integer int1) {
        Assert.assertTrue(response.body().jsonPath().getString("error.code").equals("101"));

    }

    @Then("verify that {string} city weather info is not shown.")
    public void verify_that_city_weather_info_is_not_shown(String string) {
        response.then().assertThat().body("current", Matchers.not(Matchers.hasKey("weather_descriptions")));

    }

    @Then("verify error info is {string}")
    public void verify_error_info_is(String string) {
        Assert.assertTrue(response.body().jsonPath().getString("error.info").equals(string));

    }



    @Then("{string} info is given.")
    public void info_is_given(String string) {
        response.prettyPrint();
        Assert.assertTrue(response.body().jsonPath().getString("error.info").equals(string));

    }


    @Given("user request {string} weather status with {string} historical date")
    public void userRequestWeatherStatusWithHistoricalDate(String city, String date) {
        response = RestAssured.given().accept(ContentType.JSON)
                .queryParam("access_key", "13171e3eb6db6a4ee978b6cbbc73d615")
                .queryParam("query", city)
                .queryParam("historical_date",date)
                .when().get("http://api.weatherstack.com/historical");


    }
}
